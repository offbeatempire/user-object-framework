=== User Object Framework ===
Contributors: jmdodd
Tags: user, object, relationship, framework
Requires at least: WordPress 3.4
Tested up to: 3.4.2
Stable tag: 0.6

Provide a framework for assignment of user-object relationship metadata.

== Description ==

This plugin creates a user-object relationship table to which user-object relationship
metadata may be added. This is a framework; it is intended for use in voting, flagging,
and similar user applications where a user-affiliated metadata is attached to an object.
Supported object tables include wp_posts, wp_comments, wp_users, and wp_bp_activity.

== Installation ==

1. Upload the directory `user-object-framework` and its contents to the `/wp-content/plugins/` directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.

== Changelog ==

= 0.6 =
* Initial release.

== Upgrade Notice ==

= 0.6 = 
* Initial release.

== Other Notes ==

Example code:

	$relationship = ucc_uof_get_relationship( $user_id, 0, $object_id, $object_ref );
	if ( empty( $relationship ) )
		$relationship = ucc_uof_add_relationship( $user_id, 0, $object_id, $object_ref );

	// Add user_object_meta.
	if ( $mode == 'delete' ) {
		delete_metadata( 'uof_user_object', $relationship, '_your_meta_key' );
	} else {
		update_metadata( 'uof_user_object', $relationship, '_your_meta_key', true );
	}


